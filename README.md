# Diskio Pi Case

Fichiers STL et STEP permettant d'imprimer la coque du Diskio Pi.

STL and STEP files for printing the Diskio Pi case.

![a preview of the cad model](assemblage.png)

![another preview of the cad model](assemblage2.png)

## Récupérer les fichiers

```
git clone https://gitlab.com/DiskioPi/diskio-pi-case.git
```

## Prérequis

La modification des fichiers peut être effectuée grâce à des logiciels tels que fusion360 ou meshlab.
Pour pouvoir imprimer les pièces les plus grandes, une imprimante 400x400mm est nécessaire.
L'impression est en cours de réalisation sur une Anycubic Chiron et Tevo Tornado pour les pièces plus petites.

The modification of the files can be done using software such as fusion360 or meshlab.
To be able to print the largest parts, a 400x400mm printer is required.
Printing is in progress on an Anycubic Chiron and Tevo Tornado for smaller parts.

## Contribuer au projet

Veuillez lire le fichier [CONTRIBUTION.md](https://gitlab.com/diskiopi/diskio-pi-case/blob/master/CONTRIBUTION.md) pour connaître les détails de notre code de conduite et le processus de demande pour obtenir un 'pull'

## Historique de version

Version actuelle: 1.2

Current version: 1.2

## Wiki

Les instructions d'impression en PETG sont disponibles ici : [Wiki Diskio Pi](https://www.diskiopi.com/wiki/)

PETG printing instructions are available here : [Wiki Diskio Pi](https://www.diskiopi.com/wiki/)

## Auteurs

* **Guillaume DEBRAY** - *Travail initial* - [Guillaume](https://gitlab.com/DiskioPi)

## Licence

Ce projet est enregistré sous licence OSHW avec le numéro [FR000007](https://certification.oshwa.org/fr000007.html)

